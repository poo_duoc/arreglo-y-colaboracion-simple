/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo2;

/**
 *
 * @author Palto
 */
public class Alumno {

    private String nombre;
    private int edad;
    private Asignatura[] asignaturas;

    public Alumno() {
        this.nombre = "";
        this.edad = 0;
        //this.asignatura = new Asignatura();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Asignatura[] getAsignaturas() {
        return asignaturas;
    }

    public void setAsignaturas(Asignatura[] asignaturas) {
        this.asignaturas = asignaturas;
    }

    public void imprimeAsignaturas() {
        System.out.println("***** ASIGNATURAS *******");
        for (Asignatura aux : asignaturas) {
            System.out.println("Nombre Asignatura: " + aux.getNombre());
        }
    }

    public void imprimeAsignatura(int indice) {
        if (indice < asignaturas.length && indice >= 0) {
            System.out.println("Nombre Asignatura: " + asignaturas[indice].getNombre());
        }else{
            System.out.println("Indice no correspondes");
        }
    }
    
    public Asignatura buscarAsignatura(String codAsignatura){
        Asignatura asig = null;
        for(Asignatura aux : asignaturas){
            if(aux.getCodigo().equals(codAsignatura)){
                asig = aux;
                break;
            }
        }
        return asig;
    }

}
