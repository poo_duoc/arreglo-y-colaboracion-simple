/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplo2;

/**
 *
 * @author Palto
 */
public class Asignatura {
    
    private String nombre;
    private String codigo;
    private double [] notas;
    
    public Asignatura() {
        this.nombre = "";
    }

    public Asignatura(String nombre, String codigo) {
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public double[] getNotas() {
        return notas;
    }

    public void setNotas(double[] notas) {
        this.notas = notas;
    }
    
    public void imprimeNotas(){
        for(double aux : notas){
            System.out.println("Nota : " + aux);
        }
    }
    
}
