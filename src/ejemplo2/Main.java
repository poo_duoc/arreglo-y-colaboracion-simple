/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo2;

/**
 *
 * @author Palto
 */
public class Main {

    public static void main(String[] arg) {
        Alumno alumno = new Alumno();
        int edad = alumno.getEdad();
        String nombre = alumno.getNombre();
        //Asignatura asignatura = alumno.getAsignatura();

        System.out.println("edad: " + edad);
        System.out.println("nombre: " + nombre);
        //System.out.println("asignatura: " + asignatura);

        alumno.setEdad(15);
        alumno.setNombre("Juan");

        //Asignatura asig2 = new Asignatura("MDD");
        Asignatura asig2[] = new Asignatura[2];
        asig2[0] = new Asignatura("POO", "2201");
        asig2[1] = new Asignatura("MDD", "1212");
        
        

        alumno.setAsignaturas(asig2);
        alumno.imprimeAsignaturas();
        
        Asignatura asigEncontrado = alumno.buscarAsignatura("1212");
        if(asigEncontrado == null){
            System.out.println("Curso NO encontrado");   
            
        }else{
            System.out.println("Curso Encontrado");
            asigEncontrado.setNombre("MDD.2");
            double [] notas = new double[100];
            for(int x = 0 ; x < 100 ; x++){
                notas[x] = 7.0;
            }
            
            asigEncontrado.setNotas(notas);
            asigEncontrado.imprimeNotas();
        }
        alumno.imprimeAsignaturas();
        
    }

}
